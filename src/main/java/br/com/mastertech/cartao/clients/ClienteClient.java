package br.com.mastertech.cartao.clients;

import br.com.mastertech.cartao.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "cliente")
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Optional<Cliente> getClienteById(@PathVariable Long id);

}
