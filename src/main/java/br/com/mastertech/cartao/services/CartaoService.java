package br.com.mastertech.cartao.services;


import br.com.mastertech.cartao.models.Cartao;
import br.com.mastertech.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;

    public Cartao salvar(Cartao cartao){
        Cartao cartaoObjeto = cartaoRepository.save(cartao);

        return  cartaoObjeto;
    }

    public Cartao buscarPorNumero(String numeroCartao){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numeroCartao);

        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }
        throw new RuntimeException("Cartão inexistente");
    }

    public Cartao alteraStatus(Cartao cartao, Boolean ativo){
        cartao.setAtivo(ativo);
        Cartao cartaoObjeto = salvar(cartao);
        return cartaoObjeto;
    }

    public Cartao buscarPorId(Long cartaoId) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(cartaoId);

        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }
        throw  new RuntimeException("O cartão informado id " + cartaoId + " não existe");
    }
}
