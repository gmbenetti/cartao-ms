package br.com.mastertech.cartao.controllers;


import br.com.mastertech.cartao.clients.ClienteClient;
import br.com.mastertech.cartao.models.Cartao;
import br.com.mastertech.cartao.models.CartaoMapper;
import br.com.mastertech.cartao.models.Cliente;
import br.com.mastertech.cartao.models.dtos.CartaoDTO;
import br.com.mastertech.cartao.models.dtos.CartaoResponseDTO;
import br.com.mastertech.cartao.models.dtos.CartaoStatusDTO;
import br.com.mastertech.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    public ResponseEntity<CartaoResponseDTO> registrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO){
        Optional<Cliente> clienteById = clienteClient.getClienteById(cartaoDTO.getClienteId());

        if(clienteById.isPresent()) {
            Cliente cliente = clienteById.get();

            Cartao cartao = cartaoMapper.transformaCartao(cartaoDTO, cliente.getId());
            Cartao cartaoObjeto = cartaoService.salvar(cartao);
            CartaoResponseDTO cartaoResponseDTO = cartaoMapper.transformaCartaoResposta(cartaoObjeto);
        return ResponseEntity.status(201).body(cartaoResponseDTO);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<CartaoResponseDTO> alteraStatus(@RequestBody @Valid CartaoStatusDTO cartaoStatusDTO,
                                                          @PathVariable(name="numero")String numeroCartao){

        Cartao cartao  = cartaoService.buscarPorNumero(numeroCartao);

        Cartao cartaoObjeto = cartaoService.alteraStatus(cartao, cartaoStatusDTO.getAtivo());

        CartaoResponseDTO cartaoResponseDTO = cartaoMapper.transformaCartaoResposta(cartaoObjeto);

        return  ResponseEntity.status(200).body(cartaoResponseDTO);
    }

    @GetMapping("/{numero}")
    public ResponseEntity<CartaoResponseDTO> consultar(@PathVariable(name="numero")String numero) {
        try {
            Cartao cartao = cartaoService.buscarPorNumero(numero);
            CartaoResponseDTO cartaoResponseDTO = cartaoMapper.transformaCartaoResposta(cartao);
            return ResponseEntity.status(200).body(cartaoResponseDTO);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/id/{idCartao}")
    public ResponseEntity<Cartao> consultarPorId(@PathVariable(name="idCartao")Long idCartao){
        try{
            Cartao cartao = cartaoService.buscarPorId(idCartao);
            return ResponseEntity.status(200).body(cartao);

        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
