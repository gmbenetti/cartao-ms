package br.com.mastertech.cartao.models;


import br.com.mastertech.cartao.models.dtos.CartaoDTO;
import br.com.mastertech.cartao.models.dtos.CartaoResponseDTO;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao transformaCartao(CartaoDTO cartaoDTO, Long clienteId){
        Cartao cartao = new Cartao(cartaoDTO.getNumero(), clienteId, false);

        return cartao;
    }

    public CartaoResponseDTO transformaCartaoResposta(Cartao cartao){
        CartaoResponseDTO cartaoResponseDTO = new CartaoResponseDTO(cartao.getId(),cartao.getNumero(), cartao.getClienteId(),
                cartao.getAtivo());

        return cartaoResponseDTO;
    }


}
